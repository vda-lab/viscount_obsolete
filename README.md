# VisCount
1,2,3,... or more things to show

## Current restrictions
### ... on the data
- Data needs to be in `data.js`.
- Data must be a hash.
- The keys of this hash should be strings, not numbers.
- The value of the objects should include a `id`, which corresponds to the key of the object itself. For example:
```
{ "Myriel": {"id": "Myriel", "group": 1 }, "Napoleon": {"id": "Napoleon", "group": 1} }
```
- There should be no `x` or `y` key in the data if you'll be creating a force-directed graph.

## Still hacks and current issues
- Code duplication in elements that can be brushed. => See [here](https://medium.com/swlh/sharing-code-between-svelte-component-instances-with-module-context-500028a3d359) for solution?
- Brush from node link diagram does not work correctly. Either you can brush (uncomment `BrushXY` in `NodeLink.svelte`), _or_ you can drag nodes. Brush _to_ node link diagram does work in either case.